# Backend take home project

### **Task 1:**

**1.1:**

Create a `/POST` route using the [Koa](https://koajs.com/) web framework called `/name` which accepts:

```yaml
{
	first_name: string
	last_name: string
}
```

saves it into the `Person` collection using [Mongoose](https://mongoosejs.com/docs/)  as an ORM, and returns an UUID 

```yaml
{
	id: uuid
}
```

**1.2:**

Publish an event to SNS 1 that consists of the body: 

```yaml
{
	id: uuid
	first_name: string
	last_name: string
}
```

**1.3:**

Subscribe a lambda to SNS 1, and create a lambda function that takes the `first_name` and `last_name` as input and publish an event to SNS 2 that consists of the body:

```yaml
{
	id: uuid
	first_name: string
	last_name: string
	full_name: string
}
```

**1.4:**

Subscribe SQS 1 to SNS 2, and setup polling in application to poll for SQS 1 

**1.5:**

Once the message from SQS 1 is received, update the document in the `Person` collection with their `full_name`

**1.6:**

Create another `/GET` route called `/name/:id` which uses the uuid returned from the `/POST` route created previously

**1.7:**

Create a [React](https://reactjs.org/) component to poll the route created in 1.6 for the `full_name` of the person given the `id`

### **Task 1 resources:**

**Things to lookout for:**

- Even though this is a simple backend task the quality of the code, and how the project is structured is still very important. For example structuring the projected in layers (layered architecture), having good names for variables, functions, and files
- Knowledge and setup of JSLint and Prettier is valued

**Libraries that will be useful:**

- [Serverless Framework](https://www.serverless.com/) to setup Lambda functions in AWS
- [BBC Consumer](https://github.com/bbc/sqs-consumer) to poll for SQS messages

### **Task 2:**

**2.1**

Create a Dockerfile for your Koa backend to create an image, then use Docker to create a container for the image that was created to run your Koa backend

**2.2** 

Instead of setting up SNS and SQS via AWS web interface, use [Pulumi](https://www.pulumi.com/) to set it up  

**2.3**

Setup [ECR](https://aws.amazon.com/ecr/) (using Pulumi or via AWS web interface) for your web application and upload the image that you have created in step 2.1 to it

**2.4**

Setup an Elastic Beanstalk service to host your image that has been uploaded to ECR in step 2.3

**2.5**

Push an image to your Elastic Beanstalk service that you have created in step 2.4

### **Task 2 resources:**

- Relevant [documentation](https://docs.aws.amazon.com/AmazonECR/latest/userguide/getting-started-cli.html) for 2.3
- A shell script that will be useful for step 2.5

```bash
APP_NAME=<your app name>
ENV_NAME=<your eb env name>
VERSION_LABEL=<your version label>
EB_S3_BUCKET=<your s3 bucket for storing EB application>

zip -r $VERSION_LABEL Dockerrun.aws.json

aws s3 cp $VERSION_LABEL.zip s3://$EB_S3_BUCKET/$APP_NAME/

aws elasticbeanstalk create-application-version \
--application-name $APP_NAME \
--version-label $VERSION_LABEL --source-bundle S3Bucket=$EB_S3_BUCKET,S3Key=$APP_NAME/$VERSION_LABEL.zip

aws elasticbeanstalk update-environment \
  --application-name $APP_NAME \
  --environment-name $ENV_NAME \
  --version-label $VERSION_LABEL
```

### **Task 3:**

**3.1**

Pick your CI/CD service of choice, for example if you're using Github use [Github Actions](https://docs.github.com/en/actions), GitLab use [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

**3.2**

Setup the CI/CD pipeline such that when a branch is merged into master, all the steps in Task 2 are replicated, the goal being that once a branch is merged into master, it will trigger a process that will update the Elastic Beanstalk environment